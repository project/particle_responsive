CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Particle Responsive Theme is Mobile-friendly Drupal 8 responsive theme. This
theme features a custom Banner, responsive layout, multiple column layouts and
is highly customizable. It also supports Google fonts, font awesome, and it is
great for any kind of business website.

 Particle Responsive Theme is developed with all latest technologies Drupal 8,
 Bootstrap, Font Awesome and particle js etc.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/particle_responsive

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/particle_responsive


 FEATURES
--------
 * Responsive, Mobile-Friendly Theme
 * Supported Bootstrap
 * In built Font Awesome
 * Mobile support (Smartphone, Tablet, Android, iPhone, etc)
 * A total of 15 block regions
 * Custom Banner with particle background
 * Sticky header
 * Drop Down main menus with toggle menu at mobile.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Particle Responsive module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

Navigate to Administration > Appearance and enable the theme.

Available options in the theme settings:

 * Hide/show and change copyright text.
 * Hide and show bottom to top scroll.
 * Change particle background image, padding, social icons in the footer.


MAINTAINERS
-----------

 * Swati Chouhan (swatichouhan012) - https://www.drupal.org/u/swatichouhan012
