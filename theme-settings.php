<?php
/**
 * Implementation of hook_form_system_theme_settings_alter()
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 *
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */

function particle_responsive_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $form['particle_responsive_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Particle Responsive Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];
  $form['particle_responsive_settings']['banner_display'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Particle banner'),
    '#default_value' => theme_get_setting('banner_display','particle_responsive'),
    '#description'   => t("Check this option to show Particle Background banner in front page. Uncheck to hide."),
  ];
  $form['particle_responsive_settings']['banner'] = [
    '#type' => 'fieldset',
    '#title' => t('Front Page Particle Banner'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['particle_responsive_settings']['banner_display'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Particle banner'),
    '#default_value' => theme_get_setting('banner_display','particle_responsive'),
    '#description'   => t("Check this option to show Particle Background banner in front page. Uncheck to hide."),
  ];
  $form['particle_responsive_settings']['banner'] = [
    '#markup' => t('You can change the description and URL of banner in the following Banner settings.'),
  ];
  $form['particle_responsive_settings']['banner'] = [
    '#type' => 'fieldset',
    '#title' => t('Particle banner'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];
  $form['particle_responsive_settings']['banner']['banner_head'] = [
    '#type' => 'textfield',
    '#title' => t('Particle Banner Headline'),
    '#default_value' => theme_get_setting('banner_head','particle_responsive'),
  ];
  $form['particle_responsive_settings']['banner']['banner_desc'] = [
    '#type' => 'textarea',
    '#title' => t('Particle Banner Description'),
    '#default_value' => theme_get_setting('banner_desc','particle_responsive'),
  ];
  $form['particle_responsive_settings']['banner']['banner_url'] = [
    '#type' => 'textfield',
    '#title' => t('Particle Banner URL'),
    '#default_value' => theme_get_setting('banner_url','particle_responsive'),
  ];
  $form['particle_responsive_settings']['banner']['banner_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Particle Banner Image'),
    '#default_value' => theme_get_setting('banner_image','particle_responsive'),
    '#upload_location' => 'public://',
  ];

  $form['#submit'][] = 'particle_responsive_settings_form_submit';
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  $theme_file = drupal_get_path('theme', $theme) . '/theme-settings.php';
  $build_info = $form_state->getBuildInfo();
  if (!in_array($theme_file, $build_info['files'])) {
    $build_info['files'][] = $theme_file;
  }
  $form_state->setBuildInfo($build_info);

  //Social Icon Link
  $form['particle_responsive_settings']['social_icon'] = [
    '#type' => 'details',
    '#title' => t('Social Media Link'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['particle_responsive_settings']['social_icon']['show_social_icon'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#default_value' => theme_get_setting('show_social_icon'),
    '#description'   => t("Show/Hide social media links"),
  ];
  $form['particle_responsive_settings']['social_icon']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook Link'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['particle_responsive_settings']['social_icon']['google_plus_url'] = [
    '#type' => 'textfield',
    '#title' => t('Google plus Link'),
    '#default_value' => theme_get_setting('google_plus_url'),
  ];
  $form['particle_responsive_settings']['social_icon']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter Link'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['particle_responsive_settings']['social_icon']['linkedin_url'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin Link'),
    '#default_value' => theme_get_setting('linkedin_url'),
  ];
  $form['particle_responsive_settings']['social_icon']['pinterest_url'] = [
    '#type' => 'textfield',
    '#title' => t('Pinterest Link'),
    '#default_value' => theme_get_setting('pinterest_url'),
  ];

  // Custom submit to save the file permanent.
  // $form['#submit'][] = 'particle_responsive_settings_form_submit';


  $form['particle_responsive_settings']['copyright'] = [
    '#type' => 'details',
    '#title' => t('Copyright'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['particle_responsive_settings']['copyright']['show_copyright'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Copyright text'),
    '#default_value' => theme_get_setting('show_copyright'),
    '#description'   => t("Check this option to show Copyright text. Uncheck to hide."),
  ];

  $form['particle_responsive_settings']['copyright']['copyright_text'] = [
    '#type' => 'textfield',
    '#title' => t('Enter copyright text'),
    '#default_value' => theme_get_setting('copyright_text'),
  ];
  $form['particle_responsive_settings']['show_scrolltop'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Scroll to top arrow'),
    '#default_value' => theme_get_setting('show_scrolltop'),
    '#description'   => t("Show/Hide Scroll to top arrow frpm page bottom"),
  ];
}

function particle_responsive_settings_form_submit(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $account = \Drupal::currentUser();
  $values = $form_state->getValues();
  for ($i = 1; $i <= 3; $i++) {
    if (isset($values["slide{$i}_image"]) && !empty($values["slide{$i}_image"])) {
      // Load the file via file.fid.
      if ($file = \Drupal\file\Entity\File::load($values["slide{$i}_image"][0])) {
        // Change status to permanent.
        $file->setPermanent();
        $file->save();
        $file_usage = \Drupal::service('file.usage');
        $file_usage->add($file, 'user', 'user', $account->id());
      }
    }
  }
}

