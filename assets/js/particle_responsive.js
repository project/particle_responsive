jQuery("#block-particle-responsive-search").hide();
jQuery(".particle-bg").hide();
jQuery(".scroll-top").hide();
jQuery(".path-frontpage .particle-bg").show();
jQuery(".display-banner").siblings("header").addClass("header-with-banner");
jQuery(document).ready(function () {
	jQuery('.path-frontpage #particles-js').css('height', jQuery(window).height());
	// sticky header
	jQuery(window).scroll(function () { // this will work when your window scrolled.
		var height = jQuery(window).scrollTop(); //getting the scrolling height of window
		if (height > 100) {
			jQuery(".scroll-top").show();
			jQuery(".site-header").addClass("sticky-header");
			jQuery(".site-header").removeClass("header-with-banner");
		} else {
			jQuery(".scroll-top").hide();
			jQuery(".site-header").removeClass("sticky-header");
			jQuery(".site-header").addClass("header-with-banner");
		}
	});
	// page down arrow
	jQuery(".scrollto").click(function () { /*select class that triggers scroll*/
		jQuery('html, body').animate({
			scrollTop: jQuery("#main-content").offset().top /*class you want to scroll to!!*/
		}, 1000); /*animation time length*/
	});
	jQuery(".scroll-top").click(function () {
		jQuery(".site-header").removeClass("sticky-header");
		jQuery('html, body').animate({
			scrollTop: jQuery("#top").offset().top /*class you want to scroll to!!*/
		}, 1000); /*animation time length*/
	});
	jQuery(window).resize(function () {
		jQuery('#particles-js').css('height', jQuery(window).height());
	});
	jQuery(".search-icon").click(function () {
		jQuery("#block-particle-responsive-search").animate({
			width: "toggle"
		});
	});
});

